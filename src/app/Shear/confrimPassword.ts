import { FormGroup } from '@angular/forms';
export function ConfirmPassword( password:string , confrimPassword:string ){

  return (form:FormGroup)=>{

    const passwords = form.controls[password];
    const confrim = form.controls[confrimPassword];

    if(passwords.errors && !confrim.errors.checkConfrim){
      return;
    }

    if(passwords.valid !== confrim.valid){
      confrim.setErrors({checkConfrim: true});
    }
    else
    {
      confrim.setErrors(null);
    }
  }
}
