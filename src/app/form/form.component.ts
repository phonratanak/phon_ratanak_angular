import { Component, OnInit, NgModule } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfirmPassword } from '../Shear/confrimPassword';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  datas:any;
  formGroup:FormGroup;
  submited=false;
  Check: Array<any> = [
    { name: 'A Friednd or colleauge', value: 'A Friednd or colleauge' },
    { name: 'Google', value: 'Google' },
    { name: 'Blog Post', value: 'Blog Post' },
    { name: ' New Aricle', value: 'New Aricle' },
  ];

  // date array
  day:Array<any>=[{day:'Monday'},{day:'Tuesdays'},{day:'Wednesday'},{day:'Thurday'},{day:'Friday'},{day:'Saturdays'},{day:'Saturdays'}];
  month:Array<any>=[{month:'January'},{month:'February'},{month:'March'},{month:'April'},{month:'May'},{month:'June'},{month:'July'},{month:'August'},{month:'September'},{month:'October'},{month:'November'},{month:'December'}];
  year:Array<any>=[{year:'2016'},{year:'2017'},{year:'2018'},{year:'2019'},{year:'2020'},{year:'2021'}];

  constructor(private bl:FormBuilder,config: NgbModalConfig, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.formGroup=this.bl.group({
      fristname:['',[Validators.required,Validators.maxLength(32)]],
      lastname:['',[Validators.required,Validators.maxLength(32)]],
      email: ['', [Validators.required, Validators.email]],
      areacode:['',[Validators.required,Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      phonenumber:['',[Validators.required,Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      passwords:['',[Validators.required,Validators.maxLength(120),Validators.minLength(8)]],
      confirmPassword:['',[Validators.required,Validators.maxLength(120),Validators.minLength(8)]],
      address:['',Validators.required],
      addressLine:['',Validators.required],
      city:['',Validators.required],
      state:['',Validators.required],
      zip:['',Validators.required],
      country:['',Validators.required],
      day:['',Validators.required],
      month:['',Validators.required],
      year:['',Validators.required],
      checkArray: this.bl.array([],[Validators.required]),
      agree:[false, [Validators.requiredTrue,Validators.required]],
      agrees:[false, [Validators.requiredTrue,Validators.required]],
    },{
      validator: ConfirmPassword('passwords','confirmPassword')
    });
  }


  get f(){return this.formGroup.controls;}



  onSubmit(content){
    this.submited= true;
    this.datas=this.formGroup.value;
    if(this.formGroup.invalid)
    {return;}
    this.modalService.open(content);
  }


  onCheckboxChange(e) {
    const checkArray: FormArray = this.formGroup.get('checkArray') as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
}
